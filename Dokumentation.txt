/*
  Kreise sollen allgemein gesetzt werden, um den 
  gesetzten als Marker für die darauf folgenden zu verwenden.

  1. Kreise auf den Seiten platzieren:
    - Lebendige Kreise auf die linke Seite
    - Tote Kreise auf die rechte Seite
  
  2. Jeder Kreis hat eine andere Größe:
    - Wie mache ich das mit so wenig Arbeit wie nur möglich?

-------------------- TODO: Derzeitige Probleme --------------------
  
-------------------- Geloeste Probleme --------------------
  1. Wird der erste Kreis erzeugt, bekommt dieser keine Farbe.
     Erst der zweite Kreis bekommt die zugewiesene Farbe.
      -> Loesung durch, dass ueberdecken des ersten Kreises!

  2. Die Kreise werden, mit Hilfe der FOR-Schleife gezeichnet,
     jedoch laesst sich nicht herunter scrollen obwohl weiterer
     Inhalt generiert ist.
      -> Ich lasse vorher ausrechnen wie viel Platz benötigt wird.
         Da Ich das schon vorher weiß, kann ich das als Konstante anlegen.

  3. Toten Kreise lassen sich nicht responsiv an die rechte Wand
     setzen.
      -> Die Variable <spacer> hat beim weiteren Aufrufen für die TotenKreise
         weiter gezählt. Somit musste ein weitere Spacer für die lebendemKreise
         angelegt werden.

  4. Um die Webseite nicht bei jeder Aenderung komplett ueberarbeiten zu muessen
     werde ich einen TextdateiReader einbauen. So, dass alle Veraenderungen
     durch das Einlesen der Datei passieren.
     Wie Baue ich diesen TextdateiReader ein?
      -> Die Werte lege ich erstmal in einem Array an.
         Wenn ich spaeter noch Zeit habe lege ich es generisch an,
         so das es durch das Einlesen der Dateien funktioniert.

  5. Jeder Kreis der erzeugt wird, hat auch seinen eigenen <inCanvas> Wert.
     Dieser muss für jeden Kreis selbstaendig errechnet werden.
      -> In die Zeile, in der die Klasse erzeugt wird, werden die Werte, 
         der Variablen immer wieder von neu errechnet alles was dynamisch sein muss,
         wird dynamisch errechnet.

  6. Alle, durch die For-Schleife erzeugten Kreise sollen in einem Array einzeln abrufbar sein.
     Um diese z.B. animieren zu können.
      -> Array.push(Objekt);
  
  7. Wie bekomme ich Zugriff auf das Radius Attribut meines Objektes, welches in meinem Array liegt?
      -> aliveCricleElements[0].radius?

  8. Wenn die Maus ueber einer Kugel ist, sollen: Das Jahr & die Anzahl gezeigt werden.
      -> Jedes einzelne Kreis-Objekt muss dafür in einem Array gespeichert werdem, damit auf diese 
         einfacher zugegriffen werden kann.

  9. Design und Startseite werden verändert:
      -> Der Hintergund wird schwarz. - background(0, 0, 0)
      -> Die Kreise treffen sich in der Mitte, dabei liegt der rosane oben und
         der schwarze darüber

  10. Ich brauche den Hover-Effekt auf jedem Kreis.
      Es sollen sowohl Jahr & Daten dabei gezeigt.
      -> Eine Formel macht es möglich.
------------------- Für den Print -----------------------
 * 628 px sind die 50er lebendig
 * 12 px sind die 50er Tod
 * Fact Lebendgeborene / Gestorbene -> 11368861 / 221245
 * 
 * 472 px sind die 80er Lebendig
 * 2,2 px sind die 80er Tod
 * Fact Lebendgeborene / Gestorbene -> 8532845 / 39599
 * 
 * 360 px sind die 2010er Lebendig
 * 1,3 px sind die 2010er Tod      
 * Fact Lebendgeborene / Gestorbene -> 6513312 / 24140
------------------- Für den Print -----------------------
*/