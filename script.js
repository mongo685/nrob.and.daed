// Definiere Variablen um extra Arbeit zu sparen:
const allgemeinerKreisRadius = 75;
var yAliveSpacer = 10; // Spacer zwischen den Kreisen - yAchse
var yDeadSpacer = 10;
const xSpacer = 25; // Damit die Kreise nicht an der Wand kleben.
const ySpacer = 15;
const canvasCentralY = (107 * (allgemeinerKreisRadius + xSpacer ) - 50 ); 
const canvasCentralX = window.innerWidth / 2 
// Years in a array:
var yearsFact = [];
for(let i = 1950; i <= 2018; i++){
  yearsFact.push(i);
}
// Liste der geborenen und verstorbenen Kinder 
var bornFact = [1116701,   1106380   ,1105084   ,1095029   ,1109743   ,1113408   ,1137169   ,1165555   ,1175870   ,1243922   ,1261614   ,1313505   ,1316534   ,1355595   ,1357304   ,1325386   ,1318303   ,1272276   ,1214968   ,1142366   ,1047737   ,1013396   , 901657    ,815969    ,805500    ,782310    ,798334    ,805496    ,808619    ,817217   ,865789   ,862100    , 861275    ,827933    ,812292    ,813803    ,848232    ,867969    ,892993    ,880459    ,905675    ,830019    ,809114    ,798447    ,769603    ,765221    ,796013    ,812173    ,785034    ,770744    ,766999    ,734475    ,719250    ,706721    ,705622    ,685795    ,672724    ,684862    ,682514    ,665126    ,677947   ,662685    ,673544    ,682069    ,714927   ,737575   ,792141    ,784901    ,787523];
var deadFact = [24857   ,24698   ,23768   ,22382   ,22512   ,22060   ,21309   ,20448   ,19470   ,19741   ,19814   ,19387   ,18748   ,18276   ,17565   ,16566   ,15569   ,14364   ,13505   ,12302   ,10853   ,10010   ,8415    ,7324    ,6848    ,6120    ,5882    ,5486    ,5297    ,4972    ,4954     ,4855    ,4409    ,4107    ,3803    ,3601    ,3547    ,3602    ,3474    ,3247    ,3202    ,2741    ,2660    ,2467    ,3113    ,3405    ,3573    ,3510    ,3190    ,3118    ,3084    ,2881    ,2700    ,2699    ,2728    ,2487    ,2420    ,2371    ,2412    ,2338    ,2466    ,2387    ,2400    ,2556    ,2597    ,2787    ,2914    ,3003    ,3030];
// Liste mit dem Inhalt meiner Daten fuer meine Kreise:
var aliveStatisticsFile = [123.41019403169813,122.26958735846944,122.12636225930228,121.01515209562486,122.64124322922501,123.0462740845087,125.67217808243399,128.80920560169275,129.94914919575865,137.46979306036084,139.42499248510282,145.15963262467363,145.49437708869937,149.81113295179267,150.0,146.472639880233,145.6898749285348,140.60328415741793,134.2699940470226,126.2465151506221,115.78876213434867,111.993628546,99.64499478377725,90.175340233286,89.01837760737463,86.45557664311016,88.2264400605907,89.0179355545994,89.36306825884253,90.31326069915066,95.68110754849319,95.27342437655824,95.18225099167171,91.49752008393108,89.76898321967666,89.93596865551122,93.74082740491446,95.92202631098118,98.6875084726782,97.30233610156606,100.08903679647301,91.7280506062017,89.41777228977443,88.2389280514903,85.0512854894703,84.56701667423069,87.96993893777666,89.75583214961424,86.75661458302635,85.1773810435982,84.76350913281034,81.16917801759959,79.48661464196671,78.10199483682358,77.98054083683536,75.78939574332648,74.34487778714275,75.68628693350936,75.42680195446266,73.50519854063644,74.92208819836972,73.23543583456618,74.43549860606025,75.37762358322085,79.0088661051614,81.51176891838527,87.54203185137597,86.74191632825071,87.03168192239912];
var deadStatisticsFile = [2.7470264583321056,2.729454860517614,2.6266775902819117,2.473506303672574,2.4878730188668126,2.4379210552683848,2.3549256467232103,2.259773786859834,2.1516918833216434,2.18164095884194,2.189708421989473,2.1425192882360915,2.0719013573967215,2.0197391299222573,1.941164249129156,1.830761568521127,1.720579914300702,1.587411515769496,1.4924806822937235,1.3595333101501212,1.1993996923312686,1.106237069956325,0.9299685258424051,0.809398631404608,0.7567943511549365,0.6763407460672037,0.6500386059423682,0.6062753811968432,0.585388387568297,0.5494715995827022,0.5474823620942693,0.536541555907888,0.4872526714722715,0.4538776869441187,0.4202816760283621,0.39795801088039234,0.3919902984150935,0.3980685240741942,0.38392283526755977,0.35883634027454425,0.3538632465534619,0.3029166642108179,0.2939650955128696,0.2726360491091163,0.34402757230509895,0.37629742489523355,0.3948636414539411,0.38790131024442576,0.35253708822783986,0.34458013827410805,0.34082268968484586,0.3183885113430742,0.2983856232649429,0.29827511007114105,0.30147999269139414,0.27484631298515294,0.2674419290004303,0.2620267825041406,0.2665578234500156,0.25837984710868017,0.2725255359153145,0.26379499360496983,0.26523166512439367,0.2824717233574793,0.2870027643033543,0.30800027112570216,0.3220354467385346,0.3318711209868976,0.334854977219547];
// Array mit meinen einzelnen Kreis-Objekten:
var aliveCricleElements = [];
var deadCricleElements = [];


// Klassen für meine Kreise:
class aliveCircle {
  constructor(xPosition, yPosition, radius, year, dataFact) {
    this.xPosition = xPosition;
    this.yPosition = yPosition;
    this.radius = radius;
    // Tatsaechliche Zahl der Lebendig geborenen & Jahreszahl
    this.year = year;
    // Data of the borned / dead circle:
    this.dataFact = dataFact;
    // Immer Wenn ein Kreis erzeugt wird, soll der Spacer um den Radius + 8 aufaddiert werden.
    yAliveSpacer += radius  + ySpacer - 12;
  }

  clicked(px, py){
    let d = dist(px, py, this.xPosition, this.yPosition);
    let aliveInfo = createElement('h4', '');
    if (d < this.radius){
      aliveInfo.style("font-family", "Helvetica, Arial, sans-serif");
      aliveInfo.html(this.showInformation());
      aliveInfo.style("color", "#89CFF0"); //#575756
      aliveInfo.position(canvasCentralX - 200, this.yPosition - 40);
    }
  }

  contains(px, py){
    let d = dist(px, py, this.xPosition, this.yPosition);
    if (d < this.radius){
      return true;
    } else {
      return false;
    }
  }

  //TODO: Year in bolt dataFact with commseparated
  showInformation(){
    var information = this.year + '<br>' + this.dataFact;
    return information;
  }

  intersects(other){
    let d = dist(this.xPosition, this.yPosition, other.xPosition, other.yPosition);
    return d < this.radius + other.radius;
  }

  radiusBack(){
    return this.radius;
  }

  xPositionBack(){
    return this.xPosition;
  }

  yPositionBack(){
    return this.yPosition;
  }
}


class deadCircle extends aliveCircle {
  constructor(xPosition, yPosition, radius, year, dataFact) {
    super(xPosition, yPosition, radius, year, dataFact);
    yDeadSpacer += radius + xSpacer - 10;
  }

  clicked(px, py){
    let d = dist(px, py, this.xPosition, this.yPosition);
    let deadInfo = createElement('h4', '');
    if (d < this.radius){
      deadInfo.html(this.showInformation());
      deadInfo.style("font-family", "Helvetica, Arial, sans-serif");
      deadInfo.style("color", "#575756");
      deadInfo.position(canvasCentralX + 160, this.yPosition - 40);
    }
  }

  //TODO: Year in bolt dataFact with commseparated
  showInformation(){
    var information = this.year + '<br>' + this.dataFact;
    return information;
  }
}

function mousePressed(){
  for(let i = 0; i < aliveCricleElements.length; i++){
    aliveCricleElements[i].clicked(mouseX, mouseY);
    deadCricleElements[i].clicked(mouseX, mouseY);
  }
}

function setup(){
  var canvas = createCanvas(window.innerWidth, canvasCentralY);
  background(0);
  
  for(var circleCounter = 0; circleCounter <= 68; circleCounter++){
    fill(137, 207, 240); //137, 207, 240
    let alive = new aliveCircle(
      canvasCentralX,
      aliveStatisticsFile[circleCounter] + yAliveSpacer,
      aliveStatisticsFile[circleCounter],
      yearsFact[circleCounter],
      bornFact[circleCounter] );
    ellipse(alive.xPosition, alive.yPosition, alive.radius, alive.radius);
    aliveCricleElements.push( alive );
    fill(0, 0, 0);
    let dead = new deadCircle(
      canvasCentralX,
      aliveCricleElements[circleCounter].yPositionBack(),
      deadStatisticsFile[circleCounter] * 50,
      yearsFact[circleCounter],
      deadFact[circleCounter] );
    ellipse(dead.xPosition, dead.yPosition, dead.radius / 50, dead.radius / 50);
    deadCricleElements.push( dead );
  }
}